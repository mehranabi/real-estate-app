<?php

use Illuminate\Support\Facades\Route;

Route::post('', 'SearchController@search');
Route::view('', 'welcome');

Route::prefix('transactions')->group(function () {
    Route::get('', 'TransactionController@index');

    Route::get('create', 'TransactionController@create');
    Route::post('create', 'TransactionController@store');
});

Route::prefix('people')->group(function () {
    Route::get('', 'PersonController@index');

    Route::get('create', 'PersonController@create');
    Route::post('create', 'PersonController@store');
});

Route::prefix('agents')->group(function () {
    Route::get('', 'AgentController@index');

    Route::get('create', 'AgentController@create');
    Route::post('create', 'AgentController@store');
});

Route::prefix('properties')->group(function () {
    Route::get('', 'PropertyController@index');

    Route::get('create', 'PropertyController@create');
    Route::post('create', 'PropertyController@store');
});

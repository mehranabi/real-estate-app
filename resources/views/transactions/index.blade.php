<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transactions</title>
</head>
<body>
<ul>
    <li><a href="/">Back</a></li>
    <li><a href="/transactions/create">Create a new transaction</a></li>
    @foreach($transactions as $transaction)
        <li>ID: {{ $transaction->id }} - {{ $transaction->created_at->toDateTimeString() }}</li>
    @endforeach
</ul>
</body>
</html>

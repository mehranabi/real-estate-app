<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>New Transaction</title>
</head>
<body>
<form method="post">
    @csrf
    <input name="price" type="number" placeholder="price">
    <input name="commission" type="number" placeholder="commission">
    <input name="seller_id" type="number" placeholder="Seller ID">
    <input name="buyer_id" type="number" placeholder="Buyer ID">
    <input name="agent_id" type="number" placeholder="Agent ID">
    <input name=property_id" type="number" placeholder="Property ID">
    <input name="type" type="text" placeholder="BuySell or Lease">
    <input name="created_at" type="date" placeholder="Creation DateTime">
    <input type="submit" value="Submit">
</form>
</body>
</html>

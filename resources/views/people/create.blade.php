<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>New Person</title>
</head>
<body>
<form method="post">
    @csrf
    <input name="first_name" type="text" placeholder="first name">
    <input name="last_name" type="text" placeholder="last name">
    <input name="national_code" type="text" placeholder="national code">
    <input type="submit" value="Submit">
</form>
</body>
</html>

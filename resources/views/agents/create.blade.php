<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>New Agent</title>
</head>
<body>
<form method="post">
    @csrf
    <input name="person_id" type="number" placeholder="Person ID">
    <input name="license_number" type="text" placeholder="license number">
    <input name="hired_at" type="date" placeholder="employment date">
    <input type="submit" value="Submit">
</form>
</body>
</html>

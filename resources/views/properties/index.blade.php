<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Properties</title>
</head>
<body>
<ul>
    <li><a href="/">Back</a></li>
    <li><a href="/properties/create">Create a new property</a></li>
    @foreach($properties as $property)
        <li>Beds: {{ $property->bedrooms }} - Baths: {{ $property->bathrooms }} - Sqft: {{ $property->sqft }}</li>
    @endforeach
</ul>
</body>
</html>

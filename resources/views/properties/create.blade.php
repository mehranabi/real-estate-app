<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>New Property</title>
</head>
<body>
<form method="post">
    @csrf
    <input name="bedrooms" type="number" placeholder="bedrooms">
    <input name="bathrooms" type="number" placeholder="bathrooms">
    <input name="sqft" type="number" placeholder="sqft">
    <input type="submit" value="Submit">
</form>
</body>
</html>

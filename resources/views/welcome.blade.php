<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Real Estate App</title>
</head>
<body>
<ul>
    <li><a href="/transactions">Transactions</a></li>
    <li><a href="/people">People</a></li>
    <li><a href="/agents">Agents</a></li>
    <li><a href="/properties">Properties</a></li>
</ul>
<hr>
@if(isset($transactions_count))
    <h1>Transactions Count: {{ $transactions_count }}</h1>
@endif
<hr>
<form method="post">
    @csrf
    <input name="days_back" type="number">
    <input type="submit" value="Submit">
</form>
</body>
</html>

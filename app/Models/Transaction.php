<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'type', 'price', 'commission', 'closing_date',
        'seller_id', 'buyer_id', 'agent_id', 'property_id',
    ];

    public function buyer(): BelongsTo
    {
        return $this->belongsTo(Person::class, 'buyer_id');
    }

    public function seller(): BelongsTo
    {
        return $this->belongsTo(Person::class, 'seller_id');
    }

    public function agent(): BelongsTo
    {
        return $this->belongsTo(Agent::class);
    }
}

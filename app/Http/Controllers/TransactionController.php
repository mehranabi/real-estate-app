<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index()
    {
        $transactions = Transaction::all();

        return view('transactions.index')
            ->with(compact('transactions'));
    }

    public function create()
    {
        return view('transactions.create');
    }

    public function store(Request $request)
    {
        $transaction = new Transaction($request->all());
        $transaction->created_at = Carbon::parse($request->created_at)->toDateTimeString();
        $transaction->save();

        return redirect('/transactions');
    }
}

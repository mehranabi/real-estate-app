<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function search(Request $request)
    {
//        $transactions_count = Transaction::where('created_at', '>=', now()->subDays($request->days_back))->toSql();
//        dd($transactions_count);

        $date = now()->subDays($request->days_back)->toDateString();
        $transactions = DB::select(
            DB::raw(
                "select * from transactions where created_at >= '".$date."'"
            )
        );

        return view('welcome')->with('transactions_count', count($transactions));
    }
}

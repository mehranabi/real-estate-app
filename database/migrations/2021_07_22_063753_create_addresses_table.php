<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->string('addressable_type', 50);
            $table->unsignedBigInteger('addressable_id');
            $table->string('country', 20)->default('Iran');
            $table->string('state', 20);
            $table->string('city', 20);
            $table->string('line', 100);
            $table->string('zip', 20);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}

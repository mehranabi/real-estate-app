<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('agent_id');
            $table->foreignId('buyer_id')->references('id')->on('people');
            $table->foreignId('seller_id')->references('id')->on('people');
            $table->enum('type', ['BuySell', 'Lease'])->default('BuySell');
            $table->unsignedDecimal('price');
            $table->unsignedDecimal('commission');
            $table->date('closing_date')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}

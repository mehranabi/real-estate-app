<?php

namespace Database\Factories;

use App\Models\Agent;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AgentFactory extends Factory
{
    protected $model = Agent::class;

    public function definition(): array
    {
        return [
            'license_number' => Str::random(6),
            'hired_at' => now()->subDays(random_int(0, 10)),
        ];
    }
}

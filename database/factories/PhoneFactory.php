<?php

namespace Database\Factories;

use App\Models\Phone;
use Illuminate\Database\Eloquent\Factories\Factory;

class PhoneFactory extends Factory
{
    protected $model = Phone::class;

    public function definition(): array
    {
        return [
            'number' => $this->faker->phoneNumber,
            'type' => $this->faker->randomElement(['Mobile', 'Home', 'Work']),
        ];
    }
}

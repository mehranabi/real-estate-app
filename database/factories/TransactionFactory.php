<?php

namespace Database\Factories;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    protected $model = Transaction::class;

    public function definition(): array
    {
        $price = random_int(1000000, 1000000);
        $commission = $price * (random_int(5, 15) / 100);

        return [
            'type' => $this->faker->randomElement(['BuySell', 'Lease']),
            'price' => $price,
            'commission' => $commission,
            'closing_date' => now()->addDays(random_int(0, 5))->subDays(random_int(0, 5)),
        ];
    }
}

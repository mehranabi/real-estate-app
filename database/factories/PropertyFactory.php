<?php

namespace Database\Factories;

use App\Models\Property;
use Illuminate\Database\Eloquent\Factories\Factory;

class PropertyFactory extends Factory
{
    protected $model = Property::class;

    public function definition()
    {
        return [
            'bedrooms' => random_int(0, 5),
            'bathrooms' => random_int(0, 5),
            'sqft' => random_int(50, 1000),
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    protected $model = Address::class;

    public function definition(): array
    {
        return [
            'country' => $this->faker->country,
            'state' => $this->faker->city,
            'city' => $this->faker->city,
            'line' => $this->faker->address,
            'zip' => $this->faker->postcode,
        ];
    }
}
